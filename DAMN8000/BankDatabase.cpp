#include "stdafx.h"
#include "BankDatabase.h"

using namespace std;

/*
Funktion i databas som h�mtar tillg�nglig m�ngd pengar per konto fr�n accounts-klassen.
*/
float BankDatabase::getAvailableBalance(int accountNumber)
{
	return accounts[accountNumber].getAvailableBalance();
}
/*
Funktion i databas som h�mtar total m�ngd pengar per konto.
*/
float BankDatabase::getTotalBalance(int accountNumber)
{
	return accounts[accountNumber].getTotalBalance();
}
/*
Funktion som tar in anv�ndares kontonummer och pin.
*/
bool BankDatabase::authenticateUser(int userAccountNumber, int userPIN)
{
	return accounts[userAccountNumber].validatePIN(userPIN);
}
/*
S�tter in pengar p� r�tt kontonummer.
*/
void BankDatabase::credit(int userAccountNumber, float amount)
{
	accounts[userAccountNumber].credit(amount);
}
/*
Tar ut pengar fr�n r�tt kontonummer.
*/
void BankDatabase::debit(int userAccountNumber, float amount)
{
	accounts[userAccountNumber].debit(amount);
}
/*
Funktion som har hand om antalet konton i databasen.
*/
int BankDatabase::size()
{
	return accounts.size();
}
/*
H�mtar in exempel-konton fr�n accounts.txt
*/
BankDatabase::BankDatabase()
{
	ifstream fs( "accounts.txt" );
	string line;
	while ( getline(fs, line) ) {
		istringstream iss( line );
		string token;
		getline(iss, token, ',');
		int account_number = stoi( token );
		getline(iss, token, ',');
		int pin_code = stoi( token );
		getline(iss, token, ',');
		int available_balance = stoi( token );
		getline(iss, token, ',');
		int total_balance = stoi( token );
		accounts.insert(std::make_pair(account_number, Account(account_number,pin_code,available_balance,total_balance)));
	}
}


BankDatabase::~BankDatabase()
{
}
