#include "stdafx.h"
#include "BalanceInquiry.h"

/*
Visar p� sk�rmen hur mycket pengar som finns p� konton, b�de tillg�ngliga och totala m�ndgen.
*/
void BalanceInquiry::execute(int accountNumber, Screen &screen, BankDatabase &bankDatabase)
{
	a_balance = bankDatabase.getAvailableBalance(accountNumber);
	t_balance = bankDatabase.getTotalBalance(accountNumber);
	stringstream ss;
	ss << "\nYour available balance: " << a_balance << "\nYour total balance: " << t_balance;
	screen.displayMessage(ss.str(), 5);


}

BalanceInquiry::BalanceInquiry()
{
}


BalanceInquiry::~BalanceInquiry()
{
}
