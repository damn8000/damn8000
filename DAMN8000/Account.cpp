#include "stdafx.h"
#include "Account.h"

/*
Kollar ifall det �r korrekt pin till r�tt anv�ndare.
*/
bool Account::validatePIN(int userPIN)
{
	if (userPIN == pin) {
		return true;
	}
	else {
		return false;
	}
}
/*
Returnerar tillg�ngliga pengar p� konton.
*/
float Account::getAvailableBalance()
{
	return availableBalance;
}
/*
Returnerar totala m�ngden pengar (obs, ej tillg�ngliga).
*/
float Account::getTotalBalance()
{
	return totalBalance;
}
/*
Funktion f�r att s�tta in pengar.
*/
void Account::credit(float amount)
{
	totalBalance += amount;
}
/*
Drar av n�r pengar tas ut
*/
void Account::debit(float amount)
{
	totalBalance -= amount;
	availableBalance -= amount;
}
/*
Constructor, tar emot accountnummber, pin, tillg�ngliga pengar och totala m�ngden pengar.
*/
Account::Account(int new_accountNumber, int new_pin, float new_availableBalance, float new_totalBalance)
{
	accountNumber = new_accountNumber;
	pin = new_pin;
	availableBalance = new_availableBalance;
	totalBalance = new_totalBalance;
}

Account::Account() //default empty constructor needed for members of unorderd_map (in BankDatabase)
{
}

Account::~Account()
{
}
