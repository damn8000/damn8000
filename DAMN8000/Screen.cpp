#include "stdafx.h"
#include "Screen.h"

using namespace std;

/*
Visar alla meddelanden fr�n "bankomaten" p� sk�rmen.
*/
void Screen::displayMessage(string msg)
{
	cout << msg;
}
/*
Styr hur l�nge ett meddelande visas p� sk�rmen.
*/
void Screen::displayMessage(string msg, int delay)
{
	Screen::displayMessage(msg);
	Sleep(delay * 1000);
}

Screen::Screen()
{
}


Screen::~Screen()
{
}
