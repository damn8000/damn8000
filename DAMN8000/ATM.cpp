#include "stdafx.h"
#include "ATM.h"
#include "Screen.h"
#include "Keypad.h"
#include "DepositSlot.h"
#include "CashDispenser.h"
#include "BalanceInquiry.h"
#include "Deposit.h"
#include "Withdrawal.h"
#include "BankDatabase.h"
#include "BalanceInquiry.h"


ATM::ATM()
{
	userAuthenticated = false;
	currentAccountNumber = 0;
}
/*
K�r bankomaten.
*/
void ATM::run() {
	while (true) {
		while (!userAuthenticated) {
			screen.displayMessage("\n\tWelcome to DAMN8000!");
			authenticateUser();
		}

		performTransactions();
		resetSession();
	}
}
/*
Om anv�ndaren inte sl�r in r�tt kontonummer + pin alternativt loggar ut.
*/
void ATM::resetSession() {
	userAuthenticated = false;
	currentAccountNumber = 0;
	system("cls");
}
/*
Autentiserar anv�ndaren samt styr den delen av menyn. Om man skriver in fel kontonummer / pin kommer ett meddelande upp.
*/
void ATM::authenticateUser() {
	screen.displayMessage("\nPlease enter your account number: ");
	int accountNumber = keypad.getInput();
	screen.displayMessage("\nEnter your PIN: ");
	int pin = keypad.getInput();

	userAuthenticated = bankDatabase.authenticateUser(accountNumber, pin);

	if (userAuthenticated)
		currentAccountNumber = accountNumber;
	else {
		screen.displayMessage("\nInvalid account number or PIN. Please try again!", 2);
		system("cls");
	}
}
/*
Funktion som har hand om alla olika val man kan g�ra i bankomaten
Kolla saldo, ta ut pengar, s�tta in pengar och loggar ut.
Error-meddelande om man v�ljer n�got ut�ver val 1-4.
*/
void ATM::performTransactions() {
	bool exitATM = false;

	while (!exitATM) {
		switch (mainMenu())
		{
		case 1:
			system("cls");
			{ BalanceInquiry balanceInquiry;
			balanceInquiry.execute(currentAccountNumber, screen, bankDatabase); }
			break;
		case 2:
			system("cls");
			{ Withdrawal withdrawal;
			withdrawal.execute(currentAccountNumber, screen, keypad, cashDispenser, bankDatabase); }
			break;
		case 3:
			system("cls");
			{ Deposit deposit;
			deposit.execute(currentAccountNumber, screen, keypad, depositSlot, bankDatabase); }
			break;
		case 4:
			system("cls");
			exitATM = true;
			break;
		default:
			screen.displayMessage("\nYou did not enter a valid selection. Try again.", 2);
			break;
		}
	}

}
/*
Bankomatens huvudmeny.
*/
int ATM::mainMenu() {
	system("cls");

	screen.displayMessage("\nMain Menu");
	screen.displayMessage("\n\t1 - View my balance");
	screen.displayMessage("\n\t2 - Withdraw cash");
	screen.displayMessage("\n\t3 - Deposit funds");
	screen.displayMessage("\n\t4 - Exit");
	screen.displayMessage("\nEnter a choice: ");

	return keypad.getInput();
}

ATM::~ATM()
{
}
