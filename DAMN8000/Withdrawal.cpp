#include "stdafx.h"
#include "Withdrawal.h"

/*
Funktion som visar och styr menyn f�r uttag.
*/
void Withdrawal::execute(int accountNumber, Screen &screen, Keypad &keypad, CashDispenser &cashDispenser, BankDatabase &bankDatabase)
{
	bool showUnavalibleOptions = false;
	float customerBalance = bankDatabase.getAvailableBalance(accountNumber);
	int atmBalance = cashDispenser.getAvailableCash();
	
	while (true)
	{
		screen.displayMessage("Select amount to withdraw\n");
		for (int i = 1; i <= 5; i++)
		{
			if ((i * 100) <= atmBalance || showUnavalibleOptions)
			{
				stringstream ss;
				ss << "\t" << i << " - " << i * 100 << " SEK" << endl;
				screen.displayMessage(ss.str());
			}
		}
		screen.displayMessage("\t6 - Exit\n\n");
		screen.displayMessage("Enter choice: ");

		int selection = keypad.getInput();

		if (selection <= atmBalance / 100 || (showUnavalibleOptions && selection < 5) )
		{
			int selectedAmount = selection * 100;
			if (selectedAmount <= customerBalance)
			{
				if (selectedAmount <= atmBalance)
				{
					bankDatabase.debit(accountNumber, selectedAmount);
					cashDispenser.dispenseCash(selectedAmount);
					screen.displayMessage("Take your cash. Spend them wisely", 2);
					break;
				}
				else
				{
					screen.displayMessage("Insufficient cash in ATM", 2);
					system("cls");
				}
			}
			else
			{
				screen.displayMessage("Insufficient funds on account", 2);
				system("cls");
			}			
		}
		else if (selection == 6)
		{
			screen.displayMessage("Canceling transaction..", 2);
			system("cls");
			break;
		}
		else
		{
			screen.displayMessage("You did not enter a valid selection. Try again.", 2);
			system("cls");
		}
	}
}

Withdrawal::Withdrawal()
{
}


Withdrawal::~Withdrawal()
{
}
