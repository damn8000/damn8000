#pragma once

#include <unordered_map>
#include <sstream>
#include <string>
#include <fstream>
#include "Account.h"

class BankDatabase
{
public:
	float getAvailableBalance(int accountNumber);

	float getTotalBalance(int accountNumber);

	bool authenticateUser(int userAccountNumber, int userPIN);

	void credit(int userAccountNumber, float amount);

	void debit(int userAccountNumber, float amount);

	int size();

	BankDatabase();
	~BankDatabase();
private:
	std::unordered_map<int, Account> accounts;
};

