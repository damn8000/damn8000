#pragma once
#include "Screen.h"
#include "Keypad.h"
#include "DepositSlot.h"
#include "CashDispenser.h"
#include "BalanceInquiry.h"
#include "Deposit.h"
#include "Withdrawal.h"
#include "BankDatabase.h"
#include "BalanceInquiry.h"
class ATM
{
private:
	bool userAuthenticated;
	int currentAccountNumber;
	Screen screen;
	Keypad keypad;
	DepositSlot depositSlot;
	CashDispenser cashDispenser;
	BankDatabase bankDatabase;

	void authenticateUser();
	void performTransactions();
	int mainMenu();
	void resetSession();
public:
	ATM();
	void run();
	~ATM();
};

