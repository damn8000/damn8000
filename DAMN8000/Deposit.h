#pragma once
#include "BankDatabase.h"
#include "Screen.h"
#include <iomanip>
#include <sstream>
#include "Keypad.h"
#include "DepositSlot.h"

class Deposit
{
private: 
	float amount;
public:
	void execute(int, Screen &, Keypad &, DepositSlot &, BankDatabase &);

	Deposit();
	~Deposit();
};

