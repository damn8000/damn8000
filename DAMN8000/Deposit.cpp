#include "stdafx.h"
#include "Deposit.h"

/*
Funktion f�r att s�tta in pengar p� konton.
*/
void Deposit::execute(int accountNumber, Screen &screen, Keypad &keypad, DepositSlot &depositSlot, BankDatabase &bankDatabase)
{
	screen.displayMessage("\nPlease enter deposit amount or type 0 to cancel the transaction: ");

	amount = keypad.getInput();
	if (amount != 0) {
		screen.displayMessage("\nPlease insert envelope into deposit slot.", 2);

		if (depositSlot.isEnvelopeReceived()) {
			bankDatabase.credit(accountNumber, amount);
			screen.displayMessage("\nThank you for your deposit.", 2);
		}			
	}
	else {
		screen.displayMessage("\nCanceling transaction..", 2);
	}
}

Deposit::Deposit()
{
}


Deposit::~Deposit()
{
}
