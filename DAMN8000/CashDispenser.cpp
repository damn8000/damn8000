#include "stdafx.h"
#include "CashDispenser.h"

/*
Funktion som "skriver ut" pengar med ljudeffekt!
*/
void CashDispenser::dispenseCash(float amount)
{
	for (int i = 0; i < amount; i += 100)
	{
		std::cout << "100 kr\n";
		Beep(523, 100);
		Sleep(400);
	}
	count -= amount;
}
/*
Funktionen kollar om bankomaten har tillräckligt mycket pengar tillgängliga för att skriva ut.
*/
bool CashDispenser::isSufficientCash(float amount)
{
	if (amount <= count)
	{
		return true;
	}
	else
	{
		return false;
	}
}
/*
Funktionen returnerar tillgängliga beloppet i bankomaten.
*/
int CashDispenser::getAvailableCash()
{
	
	return count;
	
}

CashDispenser::CashDispenser()
{
}


CashDispenser::~CashDispenser()
{
}
