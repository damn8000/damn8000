#pragma once
class Account
{
private:
	int accountNumber;
	int pin;
	float availableBalance;
	float totalBalance;
public:
	bool validatePIN(int userPIN);

	float getAvailableBalance();
	
	float getTotalBalance();

	void credit(float amount);

	void debit(float amount);
	
	Account(int new_accountNumber, int new_pin, float new_availableBalance, float new_totalBalance);
	Account();
	~Account();
};

