#pragma once
#include <iostream>
#include <windows.h>
#include "Screen.h"

class CashDispenser
{
private:
	int count = 500;
public:
	void dispenseCash(float amount);

	bool isSufficientCash(float amount);

	int getAvailableCash();

	CashDispenser();
	~CashDispenser();
};

