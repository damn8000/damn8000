#pragma once
#include "BankDatabase.h"
#include "Screen.h"
#include <iomanip>
#include <sstream>

class BalanceInquiry
{
private:
	float a_balance;
	float t_balance;
public:
	void execute(int, Screen &, BankDatabase &);

	BalanceInquiry();
	~BalanceInquiry();
};

