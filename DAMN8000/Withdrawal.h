#pragma once
#include "BankDatabase.h"
#include "Screen.h"
#include "Keypad.h"
#include "CashDispenser.h"

class Withdrawal
{
private:
public:
	void execute(int, Screen &, Keypad &, CashDispenser &, BankDatabase &);
	Withdrawal();
	~Withdrawal();
};

